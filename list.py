#Lists

myList = ['a','b','c','d']
print(myList)
myList[0] = 'New item'
print(myList)
myList.append('new item again')
print(myList)
myList.extend(['x','y','z'])
print(myList)
item = myList.pop()
print(myList)
print(item)
myList.reverse()
print(myList)


matrix = [[1,2,3],[4,5,6],[7,8,9]]
matrix[0][0]
# List comprehension
first_col = [row[0] for row in matrix]


print(first_col)
