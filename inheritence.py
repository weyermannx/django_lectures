class Animal():

    def __init__(self):
        print("Animal Created")


    def whoAmI(self):
        print("Animal")


    def eat(self):
        print('EATING')

class Dog(Animal):

    def __init__(self):
        Animal.__init__(self)
        print("DOG CREATED")

    def bark(self):
        print("woof")

    def eat(self):
        print('DOG EATING')


mydog = Dog()
mydog.whoAmI()
mydog.eat()
mydog.bark()



# SPECIAL METHOD
class Book():
    def __init__(self,title,author,pages):
        self.title = title
        self.author = author
        self.pages = pages

    def __str__(self):
        return "Title: {}, Author: {}, Pages: {}".format(self.title,self.author,self.pages)

    def __len__(self):
        return self.pages

    def __del__(self):
        return print("a book is destroyed")

b = Book("Python","jose",200)
mylist = [1,2,3]
print(mylist)
print(len(b))
print(b)
del b
