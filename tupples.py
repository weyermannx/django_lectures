#Booleans

True
False
0
1

t = (1,2,3)
print(t[0])


t = ('a', True,123)

print(t)

#tuples are immutable
# cant do  -  t[0] = 'b'

x = set()

x.add(1)
x.add(2)
x.add(4)
x.add(4)
x.add(4)
x.add(0.1)
print(x)


converted = set([1,2,3,4,5,5,5,5,5])

print(converted)
