#####################################
### WELCOME TO YOUR OOP PROJECT #####
#####################################

# For this project you will be using OOP to create a card game. This card game will
# be the card game "War" for two players, you an the computer. If you don't know
# how to play "War" here are the basic rules:
#
# The deck is divided evenly, with each player receiving 26 cards, dealt one at a time,
# face down. Anyone may deal first. Each player places his stack of cards face down,
# in front of him.
#
# The Play:
#
# Each player turns up a card at the same time and the player with the higher card
# takes both cards and puts them, face down, on the bottom of his stack.
#
# If the cards are the same rank, it is War. Each player turns up three cards face
# down and one card face up. The player with the higher cards takes both piles
# (six cards). If the turned-up cards are again the same rank, each player places
# another card face down and turns another card face up. The player with the
# higher card takes all 10 cards, and so on.
#
# There are some more variations on this but we will keep it simple for now.
# Ignore "double" wars
#
# https://en.wikipedia.org/wiki/War_(card_game)

import random

# Two useful variables for creating Cards.
SUITE = 'H D S C'.split()
RANKS = '2 3 4 5 6 7 8 9 10 J Q K A'.split()


class Deck:
    """
    This is the Deck Class. This object will create a deck of cards to initiate
    play. You can then use this Deck list of cards to split in half and give to
    the players. It will use SUITE and RANKS to create the deck. It should also
    have a method for splitting/cutting the deck in half and Shuffling the deck.
    """

    def __init__(self):
        self.cards = self.assemble_cards()

    def shuffle_cards(self):
        random.shuffle(self.cards)

    def assemble_cards(self):
        cards = []
        for s in SUITE:
            for i,r in enumerate(RANKS):
                cards += [Card(("{}{}".format(s,r)),i)]
        return cards

    def deal_cards(self, hand1, hand2):
        to_player1 = []
        to_player2 = []
        intial_card_court = len(self.cards)
        for i in range(intial_card_court):
            if (i%2 == 0):
                to_player1.append(self.cards.pop())
            else:
                to_player2.append(self.cards.pop())
        hand1.add_cards(to_player1)
        hand2.add_cards(to_player2)
        print("The cards have been dealt")

class Card:
    def __init__(self, card_type, strength):
        self.card_type = card_type
        self.strength = strength

    def __str__(self):
        return self.card_type

class Hand:
    '''
    This is the Hand class. Each player has a Hand, and can add or remove
    cards from that hand. There should be an add and remove card method here.
    '''
    def __init__(self):
        self.cards = []

    def add_cards(self,cards):
        self.cards += cards

    def remove_card(self):
        return self.cards.pop()

    def __len__(self):
        return len(self.cards)

    def __str__(self):
        cardsString = ""
        for c in self.cards:
            cardsString += c.card_type + " "
        return cardsString




class Player:
    """
    This is the Player class, which takes in a name and an instance of a Hand
    class object. The Player can then play cards and check if they still have cards.
    """
    def __init__(self, hand, name):
        self.hand = hand
        self.name = name

    def play_card(self):
        return self.hand.remove_card()

    def check_card_count(self):
        return len(self.hand)

    def look_at_cards(self):
        # end of game only
        return self.hand


class Table:

    def __init__(self):
        pass
        self.player_one_stack = []
        self.player_two_stack = []


    def player_one_stack(self):
        return self.player_one_stack

    def player_two_stack(self):
        return self.player_two_stack

    def find_winner_and_hand_out_cards(self, handOne, handTwo):
        print("self.player_one_stack[-1]: {} {}".format(self.player_one_stack[-1].strength, self.player_two_stack[-1]))
        if (self.player_one_stack[-1].strength > self.player_two_stack[-1].strength):
            self.handout_cards_to_player_hand(handOne)
        elif (self.player_two_stack[-1].strength > self.player_one_stack[-1].strength):
            self.handout_cards_to_player_hand(handTwo)
        else:
            print("Its a tie")
            if (self.request_more_cards_from_hands(handOne, handTwo) > 0):
                self.find_winner_and_hand_out_cards(handOne, handTwo)

    def receive_card_from_player_one(self,card):
        self.player_one_stack.append(card)
    def receive_card_from_player_two(self,card):
        self.player_two_stack.append(card)

    def handout_cards_to_player_hand(self,hand):
        print("giving these cards: {} and {} to {}. count: {}".format(len(self.player_one_stack), len(self.player_two_stack),hand,len(self.player_one_stack) + len(self.player_two_stack)))
        self.hand_out_cards(hand, self.player_one_stack)
        self.hand_out_cards(hand, self.player_two_stack)


    def hand_out_cards(self,hand,stack):
        while (len(stack)>0):
            hand.add_cards([stack.pop()])

    def request_more_cards_from_hands(self,hand1,hand2):
        cards_dealt = 0
        for i in range(3):
            if len(hand1) > 0 and len(hand2) > 0:
                self.receive_card_from_player_one(hand1.remove_card())
                self.receive_card_from_player_two(hand2.remove_card())
                cards_dealt += 1;
        return cards_dealt


######################
#### GAME PLAY #######
######################
print("Welcome to War, let's begin...")

myDeck = Deck()
myDeck.shuffle_cards()

handOne = Hand()
handTwo = Hand()

PlayerName = input("What is your name? ")
playerOne = Player(handOne,"Computer")
playerTwo = Player(handTwo,PlayerName)
myDeck.deal_cards(handOne,handTwo)

table = Table()

while(playerOne.check_card_count() > 0 and playerTwo.check_card_count() > 0):
    table.receive_card_from_player_one(playerOne.play_card())
    table.receive_card_from_player_two(playerTwo.play_card())

    table.find_winner_and_hand_out_cards(playerOne.hand, playerTwo.hand)

print("The game has ended")
print("Player 1 has {} Cards: {}".format(playerOne.check_card_count(), playerOne.look_at_cards()))
print("Player 2 has {} Cards: {}".format(playerTwo.check_card_count(), playerTwo.look_at_cards()))

winner = ""
if (playerOne.check_card_count() > playerTwo.check_card_count()):
    winner = playerOne
else:
    winner = playerTwo

print("The winner is {}".format(winner.name))


# Use the 3 classes along with some logic to play a game of war!
