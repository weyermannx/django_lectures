def my_function(param1='default'):
    """

    THIS IS THE DOCSTRING
    """
    print("my first python function! {}".format(param1))

my_function()


def hello():
    return 'hello'

result = hello()

print(result)


def addNum(num1,num2):
    return num1+num2


result = addNum(2,3)
print(type(result))


#lambda experssion

# Filter
myList = [1,2,3,4,5,6,7,8]

def even_bool(num):
    return num%2 == 0



evens = filter(lambda num: num%2 == 0,myList)
print(evens)
print(list(evens))


st = 'hello'
st.lower()
st.upper()

tweet = "Go Sports! #Sports"
result = tweet.split('#')[-1]
print(result)

print('x' in [1,2,3,'x'])
