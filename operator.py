if 1 == 1:
    if 3<2:
        print("first block")
    elif 3==3:
        print('awesome')
    else:
        print('last')



seq = [1,2,3,4,5,6]

for item in seq:
    print(item)
    print('hello')


d = {"sam":1, "frank":2, "Dan":3}

for item in d:
    print(item)
    print(d[item])


print('---------')

mypairs = [(1,2),(3,4),(5,6)]

for (tup1,tup2) in mypairs:
    print(tup2)
    print(tup1)

i = 1

while i<=5:
    print("i is {}".format(i))
    i = i+1


for item in range(20):
    print(item)


x = [1,2,3,4]
out = [num**2 for num in x]

print(out)
